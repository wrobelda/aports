From 4a9e447c49729ed893ac37749ec1b8598a11695c Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?Guido=20G=C3=BCnther?= <agx@sigxcpu.org>
Date: Thu, 22 Sep 2022 12:39:22 +0200
Subject: [PATCH 1/3] log: Track log domains instead of passing it to the
 handler as user data
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

This will allow us to not set the handler multiple times in the
next commit.

Signed-off-by: Guido Günther <guido.gunther@puri.sm>
Part-of: <https://gitlab.gnome.org/World/Phosh/phosh/-/merge_requests/1148>
---
 src/log.c                      | 39 ++++++++++++++++++++++------------
 src/main.c                     |  1 +
 tests/test-gtk-mount-manager.c |  1 +
 tests/test-idle-manager.c      |  1 +
 tests/testlib-full-shell.c     |  1 +
 5 files changed, 30 insertions(+), 13 deletions(-)

diff --git a/src/log.c b/src/log.c
index cb2dde08..8e6770b2 100644
--- a/src/log.c
+++ b/src/log.c
@@ -18,6 +18,10 @@
 /* these are filtered by G_MESSAGES_DEBUG by the default log handler */
 #define INFO_LEVELS (G_LOG_LEVEL_INFO | G_LOG_LEVEL_DEBUG)
 
+
+static char          *_log_domains;
+G_LOCK_DEFINE_STATIC (_log_domains);
+
 static gboolean
 log_is_old_api (const GLogField *fields,
                 gsize            n_fields)
@@ -42,7 +46,7 @@ static GLogWriterOutput
 phosh_log_writer_default (GLogLevelFlags   log_level,
                           const GLogField *fields,
                           gsize            n_fields,
-                          gchar           *log_domains)
+                          gpointer         unused)
 {
   static gsize initialized = 0;
   static gboolean stderr_is_journal = FALSE;
@@ -51,12 +55,15 @@ phosh_log_writer_default (GLogLevelFlags   log_level,
   g_return_val_if_fail (fields != NULL, G_LOG_WRITER_UNHANDLED);
   g_return_val_if_fail (n_fields > 0, G_LOG_WRITER_UNHANDLED);
 
-  /* Disable debug message output unless specified in log_domains. */
+  G_LOCK (_log_domains);
+
+  /* Disable debug message output unless specified in _log_domains. */
   if (!(log_level & DEFAULT_LEVELS) && !(log_level >> G_LOG_LEVEL_USER_SHIFT)) {
     const gchar *log_domain = NULL;
     gsize i;
 
-    if ((log_level & INFO_LEVELS) == 0 || log_domains == NULL) {
+    if ((log_level & INFO_LEVELS) == 0 || _log_domains == NULL) {
+      G_UNLOCK (_log_domains);
       return G_LOG_WRITER_HANDLED;
     }
 
@@ -67,8 +74,9 @@ phosh_log_writer_default (GLogLevelFlags   log_level,
       }
     }
 
-    if (strcmp (log_domains, "all") != 0 &&
-        (log_domain == NULL || !strstr (log_domains, log_domain))) {
+    if (strcmp (_log_domains, "all") != 0 &&
+        (log_domain == NULL || !strstr (_log_domains, log_domain))) {
+      G_UNLOCK (_log_domains);
       return G_LOG_WRITER_HANDLED;
     }
   }
@@ -88,14 +96,15 @@ phosh_log_writer_default (GLogLevelFlags   log_level,
   }
 
   if (stderr_is_journal &&
-      g_log_writer_journald (log_level, fields, n_fields, log_domains) ==
+      g_log_writer_journald (log_level, fields, n_fields, _log_domains) ==
       G_LOG_WRITER_HANDLED)
     goto handled;
 
-  if (g_log_writer_standard_streams (log_level, fields, n_fields, log_domains) ==
+  if (g_log_writer_standard_streams (log_level, fields, n_fields, _log_domains) ==
       G_LOG_WRITER_HANDLED)
     goto handled;
 
+  G_UNLOCK (_log_domains);
   return G_LOG_WRITER_UNHANDLED;
 
 handled:
@@ -104,21 +113,25 @@ handled:
     _phosh_log_abort (!(log_level & G_LOG_FLAG_RECURSION));
   }
 
+  G_UNLOCK (_log_domains);
   return G_LOG_WRITER_HANDLED;
 }
 
 /**
  * phosh_log_set_log_domains:
- * @domains: comma separated list of log domains. This string must
- *  remain valid during the programs life time.
+ * @domains: comma separated list of log domains.
  *
- * Set the current logging domains.
+ * Set the current logging domains. This sets an appropriate log
+ * handler as well.
  */
 void
 phosh_log_set_log_domains (const char *domains)
 {
-  /* We replace the whole writer instead of just updating domains
-     since that is already mutex protected */
+  G_LOCK (_log_domains);
+  g_free (_log_domains);
+  _log_domains = g_strdup (domains);
+  G_UNLOCK (_log_domains);
+
   g_log_set_writer_func ((GLogWriterFunc)phosh_log_writer_default,
-                         (char*)domains, NULL);
+                         NULL, NULL);
 }
diff --git a/src/main.c b/src/main.c
index 6f10f261..3d05841e 100644
--- a/src/main.c
+++ b/src/main.c
@@ -142,5 +142,6 @@ int main(int argc, char *argv[])
 
   cui_uninit ();
 
+  phosh_log_set_log_domains (NULL);
   return EXIT_SUCCESS;
 }
diff --git a/tests/test-gtk-mount-manager.c b/tests/test-gtk-mount-manager.c
index 0f6f1c20..8b3f1c3a 100644
--- a/tests/test-gtk-mount-manager.c
+++ b/tests/test-gtk-mount-manager.c
@@ -84,6 +84,7 @@ comp_and_shell_thread (gpointer data)
   while (g_main_context_pending (NULL))
     g_main_context_iteration (NULL, FALSE);
 
+  phosh_log_set_log_domains (NULL);
   return NULL;
 }
 
diff --git a/tests/test-idle-manager.c b/tests/test-idle-manager.c
index fca66182..309eeae5 100644
--- a/tests/test-idle-manager.c
+++ b/tests/test-idle-manager.c
@@ -82,6 +82,7 @@ comp_and_shell_thread (gpointer data)
   while (g_main_context_pending (NULL))
     g_main_context_iteration (NULL, FALSE);
 
+  phosh_log_set_log_domains (NULL);
   return NULL;
 }
 
diff --git a/tests/testlib-full-shell.c b/tests/testlib-full-shell.c
index 0f4c8604..1948f82a 100644
--- a/tests/testlib-full-shell.c
+++ b/tests/testlib-full-shell.c
@@ -82,6 +82,7 @@ phosh_test_full_shell_thread (gpointer data)
   while (g_main_context_pending (NULL))
     g_main_context_iteration (NULL, FALSE);
 
+  phosh_log_set_log_domains (NULL);
   return NULL;
 }
 
-- 
2.37.2

